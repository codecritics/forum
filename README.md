# Codecritics - Forum
> 

<!--- 
TODO I will generate the badges.
    [![NPM Version][npm-image]][npm-url]
    [![Build Status][travis-image]][travis-url]
    [![Downloads Stats][npm-downloads]][npm-url]
--->

A forum is a deceptively complex thing. Sure, it's made up of threads and replies, but what else might exist as part of 
a forum?

![](public/logo.png)

## Installation

First you need to clone this Repository.
##### ◉ Sans Docker
OS X & Linux: 
> On the root folder
```sh
$ cp .env.example .env
```
```sh
$ composer install && npm install
```
```sh
$ php artisan key:generate
```
```sh
$ php artisan migrate:seed --refresh
```
```sh
$ php artisan serve
```

Windows:
> Work in Progress
<!---
```sh
edit autoexec.bat
```
-->
## Usage example

A few motivating and useful examples of how your product can be used. Spice this up with code blocks and potentially more screenshots.

_For more examples and usage, please refer to the [Wiki][wiki]._

## Development setup
Work in progess.
<!---
Describe how to install all development dependencies and how to run an automated test-suite of some kind. Potentially do this for multiple platforms.

```sh
make install
npm test
```
-->
## Release History
<!---
* 0.2.1
    * CHANGE: Update docs (module code remains unchanged)
* 0.2.0
    * CHANGE: Remove `setDefaultXYZ()`
    * ADD: Add `init()`
* 0.1.1
    * FIX: Crash when calling `baz()` (Thanks @GenerousContributorName!)
* 0.1.0
    * The first proper release
    * CHANGE: Rename `foo()` to `bar()`
    --->
* 0.0.1
    * Work in progress

## Meta

Tanawa Tsamo Marius– [@Gitlab](https://gitlab.com/codecritics) – tanawa_m@epita.fr

Distributed under the MIT license. See ``LICENSE`` for more information.

[https://gitlab.com/codecritics/forum](https://gitlab.com/codecritics/forum)

## Contributing

1. Fork it (<https://gitlab.com/codecritics/forum>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request

<!-- Markdown link & img dfn's -->
[npm-image]: https://img.shields.io/npm/v/datadog-metrics.svg?style=flat-square
[npm-url]: https://npmjs.org/package/datadog-metrics
[npm-downloads]: https://img.shields.io/npm/dm/datadog-metrics.svg?style=flat-square
[travis-image]: https://img.shields.io/travis/dbader/node-datadog-metrics/master.svg?style=flat-square
[travis-url]: https://travis-ci.org/dbader/node-datadog-metrics
[wiki]: https://gitlab.com/codecritics/forum/wiki