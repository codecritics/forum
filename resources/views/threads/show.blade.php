@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <a href="#">{{$thread->creator->name}}</a> posted: {{$thread->title}}
                    </div>

                    <div class="card-body">
                        <article>
                            <div class="body"> {{$thread->body}}</div>
                        </article>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                @foreach($thread->replies  as $reply)
                    @include('threads.reply')
                @endforeach
            </div>
        </div>
        @if(auth()->check())
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="form-group">
                        <form action="{{$thread->path().'/replies'}}" method="POST">
                            {{csrf_field()}}
                            <label for="body"> Body: </label>
                            <textarea name="body" id="body" class="form-control"
                                      placeholder="Have something to say?"></textarea>
                            <button type="submit" class="btn btn-secondary"> Post</button>
                        </form>
                    </div>
                </div>
            </div>
        @else
            <p class="mx-auto">Please <a href="{{route('login')}}">sign in</a>to participate in this
                discussion.</p>
        @endif
    </div>
@endsection
