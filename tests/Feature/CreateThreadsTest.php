<?php

namespace Tests\Feature;

use App\Thread;
use App\User;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateThreadsTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    function guests_may_not_create_threads()
    {
        $this->withExceptionHandling();
        $this->get('threads/create')
            ->assertRedirect('/login');
//        $thread = make(Thread::class);

        $this->post('/threads', [])
            ->assertRedirect('/login');
    }


    /**
     * @test
     */
    function an_authenticated_user_can_create_new_forum_threads()
    {
        // Given we have a signed in user.

        $this->signIn();
        // When we hit the endpoint to create a new thead.

        $thread = make(Thread::class);
        $this->post('/threads', $thread->toArray());

        //Then, when we visit the thread page.

        $this->get($thread->path())
            ->assertSee($thread->title)
            ->assertSee($thread->body);
    }
    /**
     * @test
     */
    public function a_thread_belongs_to_a_channel()
    {
        $thread = create(Thread::class);
        $this->assertInstanceOf('App\Channel', $thread->channel);
    }


}
