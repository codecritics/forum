<?php

namespace Tests\Feature;

use App\Reply;
use App\Thread;
use App\User;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ParticipateInForumTest extends TestCase
{
    use DatabaseMigrations;


    /**
     * @test
     *
     */
    public function unauthenticated_users_may_not_add_replies()
    {
        $this->expectException(AuthenticationException::class);
        $this->post('/threads/1/replies',[]);
    }

    /**
     * @test
     */
    public function an_authenticated_user_may_participate_in_forum_threads()
    {
        // Given we have an authenticated user and an existing thread
        $this->be($user = factory(User::class)->create());

        $thread = factory(Thread::class)->create();
        // When the user adds a reply to the thread
        $reply = factory(Reply::class)->create();
        $this->post($thread->path().'/replies', $reply->toArray());
        // Then their reply should be visible on the page.
        $this->get($thread->path())->assertSee($reply->body);
    }
}
