<?php
/**
 * Created by PhpStorm.
 * User: mariustanawatsamo
 * Date: 13/04/2018
 * Time: 17:23
 */

function create($class, $attributes = [])
{
    return factory($class)->create($attributes);
}

function make($class, $attributes = [])
{
    return factory($class)->make($attributes);
}